package control;
import view.TheaterSystemGUI;
import model.SeatPrice;


public class TheaterManagement {
	private SeatPrice seatPrice;
	private TheaterSystemGUI gui;

	
	public TheaterManagement(SeatPrice seat, TheaterSystemGUI gui){
		this.seatPrice = seat;
		this.gui=gui;
		gui.setController(this);
	}
	
	
	public double[][] getSeatPrice(){
		return seatPrice.getSeatPrice();
	}
	
	

}
